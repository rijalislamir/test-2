# -*- coding: utf-8 -*-
from odoo import http

# class C:\odoo\addons\nibble(http.Controller):
#     @http.route('/c:\odoo\addons\nibble/c:\odoo\addons\nibble/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/c:\odoo\addons\nibble/c:\odoo\addons\nibble/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('c:\odoo\addons\nibble.listing', {
#             'root': '/c:\odoo\addons\nibble/c:\odoo\addons\nibble',
#             'objects': http.request.env['c:\odoo\addons\nibble.c:\odoo\addons\nibble'].search([]),
#         })

#     @http.route('/c:\odoo\addons\nibble/c:\odoo\addons\nibble/objects/<model("c:\odoo\addons\nibble.c:\odoo\addons\nibble"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('c:\odoo\addons\nibble.object', {
#             'object': obj
#         })